#!/usr/bin/env python
from subprocess import call
import datetime,os.path,hashlib,shutil
date=open('/root/.backup-www_date', 'r').read()
open('/root/.backup-www_date', 'r').close()
shutil.make_archive('/tmp/www', 'gztar', '/var/' , 'www' )
if (os.path.exists('/tmp/www_b.tar.gz')!=1) :
    shutil.make_archive('/tmp/www_b', 'gztar', '/var/backup/' , 'www-'+date+'.tar.gz' )
md5=(hashlib.md5("/tmp/www.tar.gz".encode('utf-8')).hexdigest())
md5_b=(hashlib.md5("/tmp/www_b.tar.gz".encode('utf-8')).hexdigest())
if (md5 != md5_b) :
    date=(datetime.datetime.now().strftime("%d-%H-%u"))
    open('/root/.backup-www_date', 'w').write(str(date))
    shutil.move(r'/tmp/www.tar.gz', r'/var/backup/www-'+date+'.tar.gz')
    os.remove('/tmp/www_b.tar.gz')
else :
    os.remove('/tmp/www.tar.gz')
